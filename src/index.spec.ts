import { expect } from 'chai';
import pigLatinConverter from './index';

describe('PigLatinConverter', function () {
    it('should add \'way\' to words starting with vowels', function () {
        expect(pigLatinConverter('a')).to.be.equal('away');
        expect(pigLatinConverter('anaconda')).to.be.equal('anacondaway');
        expect(pigLatinConverter('Emmanuel')).to.be.equal('Emmanuelway');
    });

    it('should throw an error if the text is empty', function () {
        expect(() => pigLatinConverter('')).to.throw('Need a non empty string to convert it to pig-latin');
    });

    it('should leave untouched words that finish with \'way\'', function () {
        expect(pigLatinConverter('way')).to.be.equal('way');
        expect(pigLatinConverter('midway')).to.be.equal('midway');
        expect(pigLatinConverter('Runway')).to.be.equal('Runway');
        expect(pigLatinConverter('subway')).to.be.equal('subway');
        expect(pigLatinConverter('Airway')).to.be.equal('Airway');
        expect(pigLatinConverter('leeway')).to.be.equal('leeway');
        expect(pigLatinConverter('anyway')).to.be.equal('anyway');
    });

    it('should concatenate first letter and add \'ay\' to consonant starting words', function () {
        expect(pigLatinConverter('hello')).to.be.equal('ellohay');
        expect(pigLatinConverter('Pig')).to.be.equal('Igpay');
        expect(pigLatinConverter('latin')).to.be.equal('atinlay');
        expect(pigLatinConverter('Banana')).to.be.equal('Ananabay');
        expect(pigLatinConverter('will')).to.be.equal('illway');
        expect(pigLatinConverter('Duck')).to.be.equal('Uckday');
    });

    it('should treat hyphens as an space', function () {
        expect(pigLatinConverter('rocket-man')).to.be.equal('ocketray-anmay');
        expect(pigLatinConverter('This-Thing')).to.be.equal('Histay-Hingtay');
    });

    it('should mantain relative positionning of apostrophes', function () {
        expect(pigLatinConverter("can't")).to.be.equal("antca'y");
        expect(pigLatinConverter("doesn't")).to.be.equal("oesntda'y");
        expect(pigLatinConverter("isn't")).to.be.equal("isntwa'y");
    });

    it('should convert large texts', function () {
        expect(pigLatinConverter(
            `
            The ash of stellar alchemy at the edge of forever encyclopaedia galactica Drake Equation citizens of distant epochs across the centuries. Shores of the cosmic ocean Flatland dispassionate extraterrestrial observer courage of our questions cosmic ocean rich in heavy atoms.
Descended from astronomers descended from astronomers concept of the number one citizens of distant epochs concept of the number one another world.
Dream of the mind's eye invent the universe with pretty stories for which there's little good evidence Apollonius of Perga the only home we've ever known how far away and billions upon billions upon billions upon billions upon billions upon billions upon billions.
            `
        )).to.be.equal(
            `
            Hetay ashway ofway tellarsay alchemyway atway hetay edgeway ofway oreverfay encyclopaediaway alacticagay Rakeday Equationway itizenscay ofway istantday epochsway acrossway hetay enturiescay. Horessay ofway hetay osmiccay oceanway Latlandfay ispassionateday extraterrestrialway observerway ouragecay ofway ourway uestionsqay osmiccay oceanway ichray inway eavyhay atomsway.
Escendedday romfay astronomersway escendedday romfay astronomersway onceptcay ofway hetay umbernay oneway itizenscay ofway istantday epochsway onceptcay ofway hetay umbernay oneway anotherway orldway.
Reamday ofway hetay indsma'y eyeway inventway hetay universeway ithway rettypay toriessay orfay hichway heresta'y ittlelay oodgay evidenceway Apolloniusway ofway Ergapay hetay onlyway omehay evew'ay everway nownkay owhay arfay away andway illionsbay uponway illionsbay uponway illionsbay uponway illionsbay uponway illionsbay uponway illionsbay uponway illionsbay.
            `
        );
    });
});