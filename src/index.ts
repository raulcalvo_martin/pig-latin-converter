function doRelativeCapitalization(word: string, latin: string): string {
    const wordLength = word.length;
    return Array.from(latin).reduce((acc: string[], letter: string, idx: number): string[] => {
        if (idx >= wordLength || word[idx] !== word[idx].toUpperCase()) {
            return [...acc, letter];
        }

        return [...acc, letter.toUpperCase()];
    }, []).join('');
}

function addSuffix(word: string): string {
    switch (word[0].toLowerCase()) {
        case 'a':
        case 'e':
        case 'i':
        case 'o':
        case 'u':
            return `${word}way`;
        default:
            return doRelativeCapitalization(word, `${word.substr(1)}${word[0].toLowerCase()}ay`);
    }
}

function pigLatinWordEncoder(word: string): string {
    if (/way$/.test(word)) {
        return word;
    }

    const [head, tail] = word.split("'");

    if (tail) {
        const letters = Array.from(addSuffix(`${head}${tail}`));
        return letters.slice(
            0, letters.length - tail.length
        ).concat(
            ["'"].concat(letters.slice(letters.length - tail.length))
        ).join('');
    }

    return addSuffix(word);
}

export default function convertToPigLatin(text: string): string {
    if (!text) {
        throw new Error('Need a non empty string to convert it to pig-latin');
    }

    return text.replace(/[A-Za-z']+/gm, pigLatinWordEncoder);
}
